
# My Counter-strike: Global Offensive Configuration Files

A collection of files that serve as my main configuration script for CS:GO.

## Todo

### Major

- [x] Make this todo
- [ ] Add bot replacement instead of placement
- [ ] Add round restart binds
- [ ] Integrate entry frag scripts
- [ ] Create demo control script
- [ ] Integrate server configs and commands with local binds and alii

### Minor
 - [ ] Remove cl_showfps
 - [ ] Add server in changed from password.cfg to private.cfg include
 - [ ] Add give weapon_decoy to button, or just condense nade buttons to give all nades